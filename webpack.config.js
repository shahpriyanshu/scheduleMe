var path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css?$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['airbnb'],
                        },
                    },
                    {
                        loader: 'react-svg-loader',
                        query: {
                            jsx: true,
                        },
                    },
                ],
            },
        ]
    },
    devServer: {
        contentBase: path.resolve(__dirname, "dist")
    }
};