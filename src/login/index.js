import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Grid, Row, Col } from 'react-bootstrap';
import 'antd/dist/antd.css';
// import './loginStyle.css';
const FormItem = Form.Item;

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogInForm: false,
            isRegisterForm: true,
            isForgotPassword: false
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Grid style={{ width: window.innerWidth, height: window.innerHeight, overflow: 'hidden' }}>
                <Row className='show-grid' style={{ width: '100vw', height: '100vh', minHeight: "100%" }}>
                    <Col md={8} xsHidden style={{ padding: 0 }}>
                        <img style={{ minHeight: window.innerHeight, maxWidth: '100%' }} src={require("../assets/img/scheduleMeLoginImage.jpeg")} alt="ScheduleMEImage" />
                    </Col>
                    <Col md={4} xs={12} sm={12} style={{ marginTop: '10%', minHeight: window.innerHeight, maxWidth: '100%' }}>
                        <h1> scheduleMe </h1>
                        <Form onSubmit={this.handleSubmit} className="form-group">
                            <FormItem>
                                {getFieldDecorator('userName', {
                                    rules: [{ required: true, message: 'Please input your email or username!' }]
                                })(
                                    <Input prefix={<Icon type='user' style={{ color: 'rgba(0, 0, 0, 0.25)' }} />} placeholder='Username or Email' />
                                    )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your password!' }]
                                })(
                                    <Input prefix={<Icon type='lock' style={{ color: 'rgba(0, 0, 0, 0.25)' }} />} type='password' placeholder='Password' />
                                    )}
                            </FormItem>
                            <FormItem>
                                <Col md={7} xs={12} style={{ padding: 0 }}>
                                    {getFieldDecorator('remember', {
                                        valuePropName: 'checked',
                                        initialValue: true,
                                    })(
                                        <Checkbox style={{ float: "left" }}>Remember me</Checkbox>
                                        )}
                                </Col>
                                <Col md={5} xs={12} style={{ padding: 0 }}>
                                    <a style={{ float: "right" }} className='login-form-forgot' href="">Forgot password</a>
                                </Col>
                                <Col md={12} style={{ padding: 0 }}>
                                    <Button type='primary' htmlType='submit' style={{ width: "100%" }}>
                                        Log in
                                </Button>
                                </Col>    
                                Or <Link to = '/signup'> Register now! </Link>
                            </FormItem>
                        </Form>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

const scheduleMeLoginForm = Form.create()(Login)

export default scheduleMeLoginForm;