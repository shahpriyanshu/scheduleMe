import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Grid, Row, Col } from 'react-bootstrap';

import signupRequest from './actions';
import 'antd/dist/antd.css';

// import './loginStyle.css';
const FormItem = Form.Item;

class Signup extends Component {
    constructor(props) {
        super(props);
    }

    registerUser = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.registerUser(values);
                console.log('Received values of form: ', values);
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Grid style={{ width: window.innerWidth, height: window.innerHeight, overflow: 'hidden' }}>
                <Row className='show-grid' style={{ width: '100vw', height: '100vh', minHeight: "100%" }}>
                    <Col md={8} xsHidden style={{ padding: 0 }}>
                        <img style={{ minHeight: window.innerHeight, maxWidth: '100%' }} src={require("../assets/img/scheduleMeLoginImage.jpeg")} alt="ScheduleMEImage" />
                    </Col>
                    <Col md={4} xs={12} sm={12} style={{ marginTop: '10%', minHeight: window.innerHeight, maxWidth: '100%' }}>
                        <h1> scheduleMe </h1>
                        <Form onSubmit={this.registerUser} className="form-group">
                            <FormItem>
                                {getFieldDecorator('userName', {
                                    rules: [{ required: true, message: 'Please input your name!' }]
                                })(
                                    <Input prefix={<Icon type='user' style={{ color: 'rgba(0, 0, 0, 0.25)' }} />} placeholder='Full name' />
                                    )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('mailId', {
                                    rules: [{ required: true, message: 'Please input your Email id!' }]
                                })(
                                    <Input prefix={<Icon type='mail' style={{ color: 'rgba(0, 0, 0, 0.25)' }} />} placeholder='Email Id' />
                                    )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your password!' }]
                                })(
                                    <Input prefix={<Icon type='lock' style={{ color: 'rgba(0, 0, 0, 0.25)' }} />} type='password' placeholder='Password' />
                                    )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('confirmPassword', {
                                    rules: [{ required: true, message: 'Please input your password again!' }]
                                })(
                                    <Input prefix={<Icon type='lock' style={{ color: 'rgba(0, 0, 0, 0.25)' }} />} type='password' placeholder='Confirm password' />
                                    )}
                            </FormItem>
                            <FormItem>
                                <Col md={12} style={{ padding: 0 }}>
                                    <Button type='primary' htmlType='submit' style={{ width: "100%" }}>
                                        Register
                                </Button>
                                </Col>
                                Or already have account <Link to="/signin"> Sign in! </Link>
                            </FormItem>
                        </Form>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return {
        signup: state.signup
    }
};

const mapDispatchToProps = dispatch => {
    return {
        registerUser: userCredentials => {
            dispatch(signupRequest(userCredentials));
        }
    }
}

const connected = connect(mapStateToProps, mapDispatchToProps)(Signup)
const scheduleMeSignupForm = Form.create()(connected)

export default scheduleMeSignupForm;