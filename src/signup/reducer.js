import { SIGNUP_REQUSTING } from './constants';

const initialState = {
    requesting: false,
    sucessful: false,
    meassage: [],
    errors: [],
}

const signupReducer = (state = initialState, action) => {
    switch(action.type) {
        case  SIGNUP_REQUSTING:
            return {
                requesting: true,
                sucessful: false,
                meassage: [{ body: "Signing up...", time: new Date()}],
                errors: [],
            }

        default: 
            return state;    
    }
}

export default signupReducer;