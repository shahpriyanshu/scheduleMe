import { combineReducers } from "redux";
import {reducer as form} from 'redux-form';
import clientReducer from './client/reducer';
import signupReducer from './signup/reducer';

const indexReducer = combineReducers(
    clientReducer,
    signupReducer,
    form
);


export default indexReducer;