import React from 'react'
import ReactDOM from 'react-dom'
import { applyMiddleware, createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { BrowserRouter, Route, Switch} from 'react-router-dom'

// Import all of our components
import App from './app'
import scheduleMeLoginForm from './login'
import scheduleMeSignupForm from './signup'
import Widgets from './widgets'
import './index.css'

// Import the index reducer and sagas
import IndexReducer from './index-reducer'
import IndexSagas from './index-sagas'

// Setup the middleware to watch between the Reducers and the Actions
const sagaMiddleware = createSagaMiddleware()

// Redux DevTools - completely optional, but this is necessary for it to
// work properly with redux saga.  Otherwise you'd just do:
//
// const store = createStore(
//   IndexReducer,
//   applyMiddleware(sagaMiddleware)
// )

/*eslint-disable */
const composeSetup = process.env.NODE_ENV !== 'production' && typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose
/*eslint-enable */
 console.log('window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__' + window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)
const store = createStore(
  IndexReducer,
  composeSetup(applyMiddleware(sagaMiddleware)), // allows redux devtools to watch sagas
)

// Log the initial state
console.log(store.getState())
 
// Every time the state changes, log it
// Note that subscribe() returns a function for unregistering the listener


// Begin our Index Saga
// sagaMiddleware.run(IndexSagas)
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
       <Switch>
         <Route exact path="/" component={scheduleMeLoginForm}/>
        <Route exact path="/signin" component={scheduleMeLoginForm} />
        <Route exact path="/signup" component={scheduleMeSignupForm} />
        <Route path="/widgets" component={Widgets} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
)
