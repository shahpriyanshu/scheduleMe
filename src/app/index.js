import React from 'react';
import PropTypes from 'prop-types';
// import logo from '../logo.svg';
import './App.css';



const App = props => (
   // console.log(`props are: ${props}`)
    <div className = 'App'>
        <div className="App-header">
                <h2>Welcome to ScheduleMe {props.children}</h2>
        </div>
        <section className='App-body'>
            {props.children}
        </section>    
    </div>
);

App.propTypes = {
    children: PropTypes.node
}


export default App;